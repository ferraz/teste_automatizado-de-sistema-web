package br.unipe.fabrica.materiais.teste;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;

import br.unipe.fabrica.materiais.Base;
import br.unipe.fabrica.materiais.LoginPage;

public class LoginTest extends Base {
	
	public LoginPage page;
	static final String SITE = "http://10.10.32.140:7903/";
	
	public LoginTest() {
		super(new ChromeDriver(), SITE);
		if (this.getDrv() == null) {
			System.exit(-1);
		} else {
			page = new LoginPage(this.getDrv(), SITE);
		}
	}
	
	
	
	@Test
	public void invalidarUsuarioSenhaLogin() {
		page.goHome();
		page.setUsuarioLogin("Testefabrica@gmail");
		page.setSenhaLogin("123456#");
		page.submit();
		assertTrue(page.falha_login());
	}
	
	@Test
	public void invalidarSenhaLogin() {
		page.goHome();
		page.setUsuarioLogin("admin@pm.pb.gov.br");
		page.setSenhaLogin("12#434");
		page.submit();
		assertTrue(page.falha_login_senha());
	}
	
	public boolean isLogged() {
		return page.validaLogin("admin@pm.pb.gov.br", "12345678"); 
	}
	
	@Test
	public void validaLogin() {
		assertTrue(isLogged());
	}
}
