package br.unipe.fabrica.materiais;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class Utils {
	
	static {
		System.setProperty("log4j.configurationFile", System.getProperty("user.dir") + "/log4j2.yaml");
	}

	private static String os = System.getProperty("os.name").toLowerCase();
	private static final Logger LOGGER = LogManager.getLogger("Fabrica_ETQS");
	
	private Utils() { throw new IllegalStateException("Utility class"); }
	

	public static String formatarData(String formato, Date data) {
		return new SimpleDateFormat(formato).format(data);
	}

	public static String dataDeHoje(String formato) {
		return formatarData(formato, new Date());
	}

	public static String now() {
		return formatarData("dd/MM/yyyy HH:mm:ss", new Date());
	}

	public static String dataDeOntem(String formato) {
		return new SimpleDateFormat(formato).format(new Date().getTime() - 86400000L);
	}

//	public static String data_ontem_Cal() {
//		return Calendar.getInstance(new Locale("pt", "br")).toString();
//	}

	public static Logger log() {
		return LOGGER;
	}

	public static boolean osIsWindows() {
		return os.contains("windows");
	}

	public static boolean osIsLinux() {
		return os.contains("linux");
	}

	public static boolean osIsMac() {
		return os.contains("mac");
	}
}
