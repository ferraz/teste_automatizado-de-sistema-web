package br.unipe.fabrica.materiais;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {
	
	private DriverManipulation _drv;
	
	static String SITE = "";
	
	public LoginPage(WebDriver drv, String site) {
		SITE = site;
		_drv = new DriverManipulation(drv);
	}
	
	// ******** Metodo para redirecionar driver para pagina inicial do teste ********
	public void goHome() { _drv.navegarPara(SITE); }

	// ******** Elementos para Login ********
	static By CAMPO_USUARIO_LOGIN = By.xpath("//*[@id=\"user_email\"]"); //By.id("user_email");
	static By CAMPO_SENHA_LOGIN = By.id("user_password");
	static By BOTAO_LOGIN_SUBMIT = By.xpath("//*[@id=\"new_user\"]/div[3]/div/input");
	
	
	// ****** M�todo para page login *******
	public void setUsuarioLogin(String text) {_drv.preencherCampo(CAMPO_USUARIO_LOGIN, text);}
	public void setSenhaLogin(String text) { _drv.preencherCampo(CAMPO_SENHA_LOGIN, text);}
	public void submit() { _drv.clique(BOTAO_LOGIN_SUBMIT);}
	
	
	// *********M�todo para login valido ********
	public boolean validaLogin(String usuarioLogin,String usuarioSenha) {
		_drv.preencherCampo(CAMPO_USUARIO_LOGIN, usuarioLogin);
		_drv.preencherCampo(CAMPO_SENHA_LOGIN, usuarioSenha);
		_drv.clique(BOTAO_LOGIN_SUBMIT);
		return sucesso_login();
	}
	
	// ****** Elementos de confirma��o do teste *******
	static By RESULTADO_LOGIN_SUCESSO = By.xpath("/html/body/div/div/section[1]/div");
	static By RESULTADO = By.xpath("/html/body/div/div[2]");
	static By RESULTADO_SENHA = By.xpath("/html/body/div/div[2]");
	
	// ******* M�todos para checagem de sucesso das opera��es ********
	public boolean sucesso_login() { 
		return _drv.checar_contem(RESULTADO_LOGIN_SUCESSO, "Login realizado com sucesso.");
	}
	
	
	
	//******M�todos para checagem de falha das opera��es ********
	public boolean falha_login() {return _drv.checar_contem(RESULTADO, "Email ou senha invalidos."); }
	public boolean falha_login_senha() {return _drv.checar_contem(RESULTADO_SENHA, "Email ou senha invalidos.");}
}
