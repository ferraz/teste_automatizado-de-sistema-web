package br.unipe.fabrica.materiais;



import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

public class TestLogger extends TestWatcher{
	
	@Override
	protected void failed(Throwable e, Description description) {
		Utils.log().error(description, e);
	}

	@Override
	protected void succeeded(Description description) {
		Utils.log().info(description);
	}
}
