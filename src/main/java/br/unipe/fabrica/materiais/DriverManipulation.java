package br.unipe.fabrica.materiais;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DriverManipulation {
	
private WebDriver drv;
	
	public DriverManipulation(WebDriver drv) {
		this.drv = drv;
	}
	public void atualizarPagina(){
		drv.navigate().refresh();
	}
	public void navegarPara(String url) {
			drv.navigate().to(url);
	}

	public void preencherCampo(By element, String text) {
		drv.findElement(element).sendKeys(text);
	}
	
	public void limparCampo(By element) {
		drv.findElement(element).clear();
	}

	public void clique(By element) {
		drv.findElement(element).click();
	}
	
	public void duploclique(By element) {
		drv.findElement(element).click();
		drv.findElement(element).click();
	}
	
	public void selecionaValorVisivel(By element, String text) {
		Select combo = new Select(drv.findElement(element));
		combo.selectByVisibleText(text);
	}
	
	public void espera(long tempo) throws InterruptedException {
		wait(tempo);
	}
	
	public boolean checar_iniciaPor(By element, String text) {
		try {
			WebElement el = drv.findElement(element);
			return el.getText().startsWith(text);
		} catch (NoSuchElementException el) {
			return false;
		}
	}
	
	public boolean checar_contem(By element, String text) {
		try {
			WebElement el = drv.findElement(element);
			return el.getText().contains(text);
		} catch (NoSuchElementException el) {
			return false;
		}
	}
	
	public boolean verificaValor(By element, String text) {
		try {
			WebElement el = drv.findElement(element);
			return el.getAttribute("value").startsWith(text);
		} catch (NoSuchElementException el) {
			return false;
		}
	}
	
	public boolean explicitWait(By element, long timeOutInSeconds) {
		WebDriverWait wait = new WebDriverWait(drv, timeOutInSeconds);
		WebElement el = wait.until(ExpectedConditions.presenceOfElementLocated(element));
		return el instanceof WebElement;
	}

	public boolean explicitWait_visibility(By element, long timeOutInSeconds) {
		WebDriverWait wait = new WebDriverWait(drv, timeOutInSeconds);
		WebElement el = wait.until(ExpectedConditions.visibilityOfElementLocated(element));
		return el instanceof WebElement && el.isDisplayed();
	}

	public boolean explicitWait_clickable(By element, long timeOutInSeconds) {
		WebDriverWait wait = new WebDriverWait(drv, timeOutInSeconds);
		WebElement el = wait.until(ExpectedConditions.elementToBeClickable(element));
		return el instanceof WebElement;
	}

}
