package br.unipe.fabrica.materiais;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Base {
	private WebDriver driver;
	private String target;
	
	// ******** JUnit Annotations
	@Rule
	public TestWatcher watchdog = new TestLogger();
	
	@BeforeClass
	public static void init() {
		Utils.log().info("\n ********************   Inicio dos Testes " + Utils.now() + "   ******************** \n");
	}
	
	@AfterClass
	public static void quit() {
		Utils.log().info("\n ********************   Fim  dos  Testes " + Utils.now() + "   ******************** \n");
	}
	
	@Before
	public void hi() { driver.get(Target()); }
	
	@After
	public void bye() { driver.close(); }
	
	// ******** constructors 
	private Base() {
		String driver = "";
		if(Utils.osIsLinux()) driver = "/geckodriver";
		if(Utils.osIsWindows()) driver = "\\geckodriver.exe";
		System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + driver);
		if(Utils.osIsLinux()) driver = "/chromedriver";
		if(Utils.osIsWindows()) driver = "\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + driver);
	}
	
	public Base(WebDriver drv, String str, long tempo) {
		this();
		driver = drv;
		driver.manage().timeouts().implicitlyWait(tempo, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		Target(str);
	}
	
	public Base(WebDriver drv, String str) { this(drv, str, 0); }
	
	public Base(String str) { this(new FirefoxDriver(), str); }

	// ******** getters & setters
	public WebDriver getDrv() { return driver;  }
	public String Target() { return target; }
	public void Target(String url) { target = url; }
}
